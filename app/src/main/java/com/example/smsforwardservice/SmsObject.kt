package com.example.smsforwardservice

class SmsObject {
    var originatingAddress: String? = null
    var content: String? = null
    var serviceCenterAddress: String? = null
}