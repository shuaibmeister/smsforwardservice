package com.example.smsforwardservice.views

import android.content.Context
import android.widget.Toast
import com.example.smsforwardservice.AndroidHelper
import com.example.smsforwardservice.localStorage.LocalStorageManager
import com.example.smsforwardservice.localStorage.SettingsStorageManager

class SettingsController(appContext: Context) {
    private val storageManager: LocalStorageManager = SettingsStorageManager(appContext)
    private val context: Context = appContext

    fun saveValidFormValues(emailAddressFieldValues: String, password: String){
        val emailAddresses = emailAddressFieldValues.replace(" ", "").split(",").toTypedArray()
        if(emailAddresses.isNotEmpty()){
            if(areEmailsValid(emailAddresses)){
                storageManager.saveToStorage(LocalStorageManager.recipient_email_key, emailAddresses.joinToString())
            } else {
                showToastMessage("Email address/es are not valid. Email will not be saved")
            }
        }
        storageManager.saveToStorage(LocalStorageManager.sender_email_password_key, password)
        showToastMessage("Valid information saved")
    }

    private fun showToastMessage(message: String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun areEmailsValid(emailAddresses: Array<String>): Boolean {
        val androidHelper = AndroidHelper()
        for(emailAddress in emailAddresses){
            if(!androidHelper.isEmailValid(emailAddress)){
                return false
            }
        }
        return true
    }

    fun getRecipientEmail(): String {
        return storageManager.getValue(LocalStorageManager.recipient_email_key)
    }

    fun getSenderEmailPassword(): String {
        return storageManager.getValue(LocalStorageManager.sender_email_password_key)
    }
}