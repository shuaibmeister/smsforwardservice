package com.example.smsforwardservice.views

import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.widget.Button
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.smsforwardservice.R


class SettingsActivity : AppCompatActivity() {
    private var recipientEmail: EditText? = null
    private var senderPassword: EditText? = null
    private var showPassword: CheckBox? = null
    private var saveSettings: Button? = null
    private var settingsController: SettingsController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        settingsController = SettingsController(this)
        setupViews()
        setupViewListeners()
        initValues()
    }

    private fun setupViews() {
        recipientEmail = findViewById(R.id.recipient_email)
        senderPassword = findViewById(R.id.sending_email_password)
        showPassword = findViewById(R.id.show_pass_checkbox)
        saveSettings = findViewById(R.id.save_settings)
    }

    private fun setupViewListeners() {
        saveSettings!!.setOnClickListener {
            settingsController!!.saveValidFormValues(recipientEmail!!.text.toString(), senderPassword!!.text.toString())
        }

        showPassword!!.setOnCheckedChangeListener{ compoundButton: CompoundButton, b: Boolean ->
            if(b){
                senderPassword!!.transformationMethod = null
            } else {
                senderPassword!!.transformationMethod = PasswordTransformationMethod()
            }
        }
    }

    private fun initValues() {
        recipientEmail!!.setText(settingsController!!.getRecipientEmail())
        senderPassword!!.setText(settingsController!!.getSenderEmailPassword())
    }
}