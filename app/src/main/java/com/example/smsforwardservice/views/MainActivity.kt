package com.example.smsforwardservice.views

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ToggleButton
import androidx.appcompat.app.AppCompatActivity
import com.example.smsforwardservice.R


class MainActivity : AppCompatActivity() {
    private var serviceStartStopButton: ToggleButton? = null
    private var mainController: MainController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainController = MainController(this)
        setupView()
        setupViewListener()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.settings_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.settings -> {
                val settingsIntent = Intent(this, SettingsActivity::class.java)
                startActivity(settingsIntent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupView() {
        serviceStartStopButton = findViewById(R.id.start_stop_sms_service)
        serviceStartStopButton?.isChecked = mainController!!.isSmsListenerServiceRunning()
    }

    private fun setupViewListener() {
        serviceStartStopButton!!.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
               mainController!!.startSmsListenerService()
            } else if (!isChecked ) {
                mainController!!.stopSmsListenerService()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mainController!!.setupPermissions()
    }
}