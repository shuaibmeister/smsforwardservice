package com.example.smsforwardservice.views

import android.content.Context
import com.example.smsforwardservice.AndroidHelper
import com.example.smsforwardservice.SmsObject
import com.example.smsforwardservice.emailSender.EmailClientParams
import com.example.smsforwardservice.emailSender.EmailObject
import com.example.smsforwardservice.emailSender.MailSender
import com.example.smsforwardservice.localStorage.LocalStorageManager
import com.example.smsforwardservice.localStorage.SettingsStorageManager
import com.example.smsforwardservice.services.ServiceManager
import com.example.smsforwardservice.services.SmsReceiveService
import com.example.smsforwardservice.services.SmsServiceManager.Companion.getInstance
import com.thinknsync.androidpermissions.PermissionHandler
import com.thinknsync.androidpermissions.PermissionManager
import com.thinknsync.objectwrappers.AndroidContextWrapper
import com.thinknsync.observerhost.TypedObserverImpl

class MainController(context: Context) {
    private val permissionManager: PermissionManager
    private val androidHelper: AndroidHelper
    private var smsServiceManager: ServiceManager<SmsReceiveService, SmsObject>? = null
    private val storageManager: LocalStorageManager
    private val context: Context

    private fun prepareSmsServiceManager(contextWrapper: AndroidContextWrapper) {
        smsServiceManager = getInstance(contextWrapper)
        smsServiceManager!!.addObserverToService(object : TypedObserverImpl<SmsObject>() {
            override fun update(smsObject: SmsObject) {
                sendSmsAsEmail(smsObject)
            }
        })
    }

    private fun sendSmsAsEmail(message: SmsObject) {
        val recipientEmails = getReceiverEmails()
        if(recipientEmails.isNotEmpty() && recipientEmails[0] != ""){
            val ccArray = recipientEmails.drop(1).toTypedArray()
            val body = "originating number: " + message.originatingAddress +
                    "<br/>receivers number: " + androidHelper.getOwnLine1Number(context) +
                    "<br/>service center: " + message.serviceCenterAddress +
                    "<br/>message body: " + message.content
            val subject = "SMS received: " + androidHelper.getTimeStringNow()
            val clientParams: EmailClientParams = getSenderEmailParam()
            val emailObject = EmailObject("test@unices.org", arrayOf(recipientEmails[0]),
                    ccArray, subject, body)
            val emailSender = MailSender(context)
            emailSender.execute(emailObject, clientParams);
        }
    }

    private fun getSenderEmailParam(): EmailClientParams{
        val emailClientParams = EmailClientParams()
        emailClientParams.host = "mail.gandi.net"
        emailClientParams.port = 587
        emailClientParams.password = storageManager.getValue(LocalStorageManager.sender_email_password_key)
        return emailClientParams
    }

    private fun getReceiverEmails(): Array<String> {
        return storageManager.getValue(LocalStorageManager.recipient_email_key).split(",").toTypedArray()
    }

    fun isSmsListenerServiceRunning(): Boolean {
        return smsServiceManager!!.isServiceRunning
    }

    fun startSmsListenerService(){
        if(!isSmsListenerServiceRunning()) {
            smsServiceManager!!.startService(SmsReceiveService::class.java)
        }
    }

    fun stopSmsListenerService(){
        if(isSmsListenerServiceRunning()) {
            smsServiceManager!!.stopService()
        }
    }

    fun setupPermissions() {
        permissionManager.isSmsPermissionGranted
        permissionManager.isImeiPermissionGranted
    }

    init {
        this.context = context
        val contextWrapper = AndroidContextWrapper(context)
        permissionManager = PermissionHandler(contextWrapper)
        androidHelper = AndroidHelper()
        storageManager = SettingsStorageManager(context)
        prepareSmsServiceManager(contextWrapper)
    }
}