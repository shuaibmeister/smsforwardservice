package com.example.smsforwardservice.localStorage

import android.content.Context
import android.content.SharedPreferences
import com.example.smsforwardservice.AndroidHelper
import com.thinkncync.cryptolib.CryptoHelper
import com.thinkncync.cryptolib.CryptoPolicy
import java.util.*

class SettingsStorageManager(context: Context) : LocalStorageManager {
    private val sharedPref: SharedPreferences
    private val cryptoPolicy: CryptoPolicy
    override val storageName: String

    override fun saveToStorage(key: String, value: String) {
        val editor = sharedPref.edit()
        editor.putString(key, cryptoPolicy.encrypt(value))
        editor.apply()
    }

    override fun getValue(key: String): String {
        return cryptoPolicy.decrypt(sharedPref.getString(key, "")!!)
    }

    override val allData: HashMap<String, String>
        get() {
            val allValues = sharedPref.all as HashMap<String, String>
            for (value in allValues.values) {
                cryptoPolicy.decrypt(value)
            }
            return allValues
        }

    init {
        val androidHelper = AndroidHelper()
        storageName = androidHelper.getApplicationId(context) + ".settings_storage"
        sharedPref = context.getSharedPreferences(storageName, Context.MODE_PRIVATE)
        cryptoPolicy = CryptoHelper(androidHelper.getStorageSecretKey())
    }
}