package com.example.smsforwardservice.localStorage

import java.util.HashMap

interface LocalStorageManager {
    val storageName: String
    fun saveToStorage(key: String, value: String)
    fun getValue(key: String): String
    val allData: HashMap<String, String>

    companion object {
        const val recipient_email_key = "recipient_email"
        const val  sender_email_password_key = "sender_email_password"
    }
}