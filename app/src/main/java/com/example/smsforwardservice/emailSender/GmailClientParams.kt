package com.example.smsforwardservice.emailSender

class GmailClientParams(userName: String, encryptedPass: String) : EmailClientParams() {
    init {
        host = "smtp.gmail.com"
        port = 587
        username = userName
        password = encryptedPass
    }
}