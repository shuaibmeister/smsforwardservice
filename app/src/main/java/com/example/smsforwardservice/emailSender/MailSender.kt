package com.example.smsforwardservice.emailSender

import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.widget.Toast

class MailSender(private val context: Context) : AsyncTask<Any, String, Boolean>() {
    private var statusDialog: ProgressDialog? = null

    override fun onPreExecute() {
        statusDialog = ProgressDialog(context)
        statusDialog!!.setMessage("Getting ready...")
        statusDialog!!.isIndeterminate = false
        statusDialog!!.setCancelable(false)
        statusDialog!!.show()
    }

    override fun doInBackground(vararg args: Any): Boolean {
        return try {
            publishProgress("Processing input....")
            val emailClient: EmailClient = BasicEmailClient((args[0] as EmailObject), (args[1] as EmailClientParams))
            emailClient.prepareEmailOptions()
            publishProgress("Preparing mail message....")
            emailClient.prepareEmailMessage()
            publishProgress("Sending email....")
            emailClient.sendEmail()
            publishProgress("Email Sent.")
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    override fun onProgressUpdate(vararg values: String) {
        statusDialog!!.setMessage(values[0])
    }

    public override fun onPostExecute(result: Boolean) {
        if (!result) {
            Toast.makeText(context, "Could not send email", Toast.LENGTH_SHORT).show()
        }
        statusDialog!!.dismiss()
    }
}