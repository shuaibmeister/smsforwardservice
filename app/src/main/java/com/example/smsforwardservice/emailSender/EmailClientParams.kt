package com.example.smsforwardservice.emailSender

open class EmailClientParams {
    var host: String? = null
    var port = 0
    var isAuth = true
    var isStarttls = true
    var username: String? = null
    var password: String? = null
}