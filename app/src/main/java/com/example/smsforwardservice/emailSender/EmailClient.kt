package com.example.smsforwardservice.emailSender

import java.io.UnsupportedEncodingException
import javax.mail.MessagingException
import kotlin.jvm.Throws

interface EmailClient {
    var emailClientParams: EmailClientParams
    var emailObject: EmailObject
    fun prepareEmailOptions()
    @Throws(MessagingException::class, UnsupportedEncodingException::class)
    fun prepareEmailMessage()
    @Throws(MessagingException::class)
    fun sendEmail()
    fun sendEmailNow()

    companion object {
        const val key_port = "mail.smtp.port"
        const val key_auth = "mail.smtp.auth"
        const val key_starttls = "mail.smtp.starttls.enable"
        const val smtp_protocol = "smtp"
    }
}