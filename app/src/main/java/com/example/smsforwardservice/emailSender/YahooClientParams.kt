package com.example.smsforwardservice.emailSender

class YahooClientParams(userName: String, encryptedPass: String) : EmailClientParams() {
    init {
        host = "smtp.mail.yahoo.com"
        port = 587
        username = userName
        password = encryptedPass
    }
}