package com.example.smsforwardservice.emailSender

import android.util.Log
import java.io.UnsupportedEncodingException
import java.util.*
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import kotlin.jvm.Throws

class BasicEmailClient(override var emailObject: EmailObject, override var emailClientParams: EmailClientParams) : EmailClient {
    private var emailProperties: Properties? = null
    private var mailSession: Session? = null
    private var emailMessage: MimeMessage? = null

    @Throws(MessagingException::class, UnsupportedEncodingException::class)
    override fun prepareEmailMessage() {
        mailSession = Session.getDefaultInstance(emailProperties, null)
        emailMessage = MimeMessage(mailSession)
        emailMessage!!.setFrom(InternetAddress(emailObject.fromAddress, emailObject.fromAddress))
        for (toAddress in emailObject.toAddresses) {
            emailMessage!!.addRecipient(Message.RecipientType.TO, InternetAddress(toAddress))
        }
        for (ccAddress in emailObject.ccAddresses) {
            emailMessage!!.addRecipient(Message.RecipientType.CC, InternetAddress(ccAddress))
        }
        emailMessage!!.subject = emailObject.subject
        emailMessage!!.setContent(emailObject.emailBody, "text/html") // for a html email
    }

    override fun prepareEmailOptions() {
        emailProperties = System.getProperties()
        emailProperties!![EmailClient.key_port] = emailClientParams.port
        emailProperties!![EmailClient.key_auth] = emailClientParams.isAuth
        emailProperties!![EmailClient.key_starttls] = emailClientParams.isStarttls
        if(emailClientParams.username == null){
            emailClientParams.username = emailObject.fromAddress
        }
    }

    @Throws(MessagingException::class)
    override fun sendEmail() {
        val transport = mailSession!!.getTransport(EmailClient.smtp_protocol)
        transport.connect(emailClientParams.host, emailObject.fromAddress, emailClientParams.password)
        transport.sendMessage(emailMessage, emailMessage!!.allRecipients)
        transport.close()
        Log.i("GMail", "Email sent successfully.")
    }

    override fun sendEmailNow() {
        try {
            prepareEmailOptions()
            prepareEmailMessage()
            sendEmail()
        } catch (e: MessagingException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }
}