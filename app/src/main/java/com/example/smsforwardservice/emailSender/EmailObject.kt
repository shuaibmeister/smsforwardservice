package com.example.smsforwardservice.emailSender

class EmailObject(var fromAddress: String, var toAddresses: Array<String>, var ccAddresses: Array<String>,
                  var subject: String, var emailBody: String)