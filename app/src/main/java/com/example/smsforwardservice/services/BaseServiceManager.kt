package com.example.smsforwardservice.services

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Build
import com.example.smsforwardservice.AndroidHelper
import com.thinknsync.objectwrappers.AndroidContextWrapper

abstract class BaseServiceManager<ServiceClassType, ObserverObjectType> protected constructor
(protected var contextWrapper: AndroidContextWrapper) : ServiceManager<ServiceClassType, ObserverObjectType> {
    protected var serviceIntent: Intent? = null
    protected var serviceClass: Class<ServiceClassType>? = null

    @SuppressLint("NewApi")
    override fun startService(service: Class<ServiceClassType>) {
        serviceClass = service
        if (!isServiceRunning) {
            serviceIntent = getServiceIntent(service)
            if (AndroidHelper().isGreaterThanOrEquals(Build.VERSION_CODES.O)) {
                contextWrapper.frameworkObject.startForegroundService(serviceIntent)
            } else {
                contextWrapper.frameworkObject.startService(serviceIntent)
            }
        }
    }

    protected abstract fun getServiceIntent(serviceClass: Class<ServiceClassType>): Intent

    override val isServiceRunning: Boolean
        get() {
            if (serviceClass == null) {
                return false
            }
            val manager = contextWrapper.frameworkObject.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(Int.MAX_VALUE)) {
                if (serviceClass!!.getName() == service.service.className) {
                    return true
                }
            }
            return false
        }

    override fun stopService() {
        contextWrapper.frameworkObject.stopService(serviceIntent)
        serviceIntent = null
    }

}