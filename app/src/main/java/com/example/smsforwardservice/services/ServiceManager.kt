package com.example.smsforwardservice.services

import com.thinknsync.observerhost.TypedObserver

interface ServiceManager<ServiceClassType, ObserverType> {
    fun startService(service: Class<ServiceClassType>)
    val isServiceRunning: Boolean
    fun stopService()
    fun addObserverToService(observer: TypedObserver<ObserverType>)
}