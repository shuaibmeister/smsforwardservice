package com.example.smsforwardservice.services

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.thinknsync.androidnotification.AndroidNotification
import com.thinknsync.androidnotification.NotificationObject
import com.thinknsync.androidnotification.NotifyUser
import com.thinknsync.objectwrappers.AndroidContextWrapper
import org.json.JSONException
import org.json.JSONObject

abstract class BaseService<T> : Service() {
    private var notificationObject = NotificationObject()
    protected var contextWrapper: AndroidContextWrapper? = null
    private val SERVICE_ID = 1111

    override fun onCreate() {
        super.onCreate()
        contextWrapper = AndroidContextWrapper(this)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent.extras != null) {
            notificationObject = try {
                NotificationObject().fromJson(JSONObject(intent.extras!!.getString(NotificationObject.tag)))
            } catch (e: JSONException) {
                e.printStackTrace()
                NotificationObject()
            }
        }

        val androidNotification: NotifyUser<Notification> = AndroidNotification(contextWrapper)
        androidNotification.init(notificationObject, null)
        onServiceCreated()
        startForeground(SERVICE_ID, androidNotification.notificationObject)
        return START_NOT_STICKY
    }

    protected abstract fun onServiceCreated()

    protected abstract fun onServiceDestroyed()

    override fun onDestroy() {
        onServiceDestroyed()
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }
}