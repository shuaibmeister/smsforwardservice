package com.example.smsforwardservice.services

import android.content.Intent
import com.example.smsforwardservice.R
import com.example.smsforwardservice.SmsObject
import com.thinknsync.androidnotification.NotificationObject
import com.thinknsync.objectwrappers.AndroidContextWrapper
import com.thinknsync.observerhost.TypedObserver

class SmsServiceManager private constructor(contextWrapper: AndroidContextWrapper) : BaseServiceManager<SmsReceiveService, SmsObject>(contextWrapper) {
    override fun addObserverToService(observer: TypedObserver<SmsObject>) {
        SmsReceiveService.addObserver(observer)
    }

    override fun getServiceIntent(service: Class<SmsReceiveService>): Intent {
        val notificationObject = serviceNotificationObject
        val serviceIntent = Intent(contextWrapper.frameworkObject, service)
        serviceIntent.putExtra(NotificationObject.tag, notificationObject.toString())
        return serviceIntent
    }

    private val serviceNotificationObject: NotificationObject
        get() {
            val notificationObject = NotificationObject()
            notificationObject.title = contextWrapper.frameworkObject.getString(R.string.app_name)
            notificationObject.content = "Listening for incoming SMS"
            notificationObject.image = R.mipmap.ic_launcher
            return notificationObject
        }

    companion object {
        private var smsServiceManager: SmsServiceManager? = null
        @JvmStatic
        fun getInstance(contextWrapper: AndroidContextWrapper): SmsServiceManager? {
            if (smsServiceManager == null) {
                smsServiceManager = SmsServiceManager(contextWrapper)
            }
            return smsServiceManager
        }
    }
}