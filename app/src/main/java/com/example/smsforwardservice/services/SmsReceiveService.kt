package com.example.smsforwardservice.services

import android.content.BroadcastReceiver
import android.content.IntentFilter
import com.example.smsforwardservice.SmsObject
import com.example.smsforwardservice.broadcastReceivers.BroadcastListener
import com.example.smsforwardservice.broadcastReceivers.SmsBroadcastReceiver
import com.thinknsync.observerhost.TypedObserver
import com.thinknsync.observerhost.TypedObserverImpl

class SmsReceiveService : BaseService<SmsObject?>() {
    private var smsBroadcastListener: BroadcastListener<SmsObject?>? = null

    override fun onServiceCreated() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(BroadcastListener.BroadcastListenerType.SMS_RECEIVE.filterStrings[0])
        smsBroadcastListener = SmsBroadcastReceiver()
        smsBroadcastListener?.addToObservers(object : TypedObserverImpl<SmsObject?>() {
            override fun update(smsObject: SmsObject?) {
//                notifyObservers(SmsObject);
                observer?.update(smsObject)
            }
        })
        registerReceiver(smsBroadcastListener as BroadcastReceiver?, intentFilter)
    }

    override fun onServiceDestroyed() {
        unregisterReceiver(smsBroadcastListener as BroadcastReceiver?)
    }

    companion object {
        private var observer: TypedObserver<SmsObject>? = null
        fun addObserver(observer: TypedObserver<SmsObject>?) {
            Companion.observer = observer
        }
    }
}