package com.example.smsforwardservice.broadcastReceivers

import android.content.BroadcastReceiver
import com.thinknsync.observerhost.ObserverHostImpl
import com.thinknsync.observerhost.TypedObserver

abstract class BaseBroadcastListener<ObserverType> : BroadcastReceiver(), BroadcastListener<ObserverType> {
    protected var observerHost: ObserverHostImpl<ObserverType> = ObserverHostImpl()

    override fun addToObservers(typedObserver: TypedObserver<ObserverType>) {
        observerHost.addToObservers(typedObserver)
    }

    override fun addToObservers(list: List<TypedObserver<ObserverType>>) {
        observerHost.addToObservers(list)
    }

    override fun removeObserver(typedObserver: TypedObserver<ObserverType>) {
        observerHost.removeObserver(typedObserver)
    }

    override fun clearObservers() {
        observerHost.clearObservers()
    }

    override fun notifyObservers(`object`: ObserverType) {
        observerHost.notifyObservers(`object`)
    }

    override fun notifyError(e: Exception) {
        e.printStackTrace()
    }
}