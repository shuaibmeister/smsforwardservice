package com.example.smsforwardservice.broadcastReceivers

import com.thinknsync.observerhost.ObserverHost

interface BroadcastListener<T> : ObserverHost<T> {
    enum class BroadcastListenerType(val receiverId: Int, val filterStrings: Array<String>) {
        SMS_RECEIVE(1, arrayOf<String>("android.provider.Telephony.SMS_RECEIVED"));
    }
}