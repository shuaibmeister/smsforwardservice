package com.example.smsforwardservice.broadcastReceivers

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import com.example.smsforwardservice.AndroidHelper
import com.example.smsforwardservice.SmsObject

class SmsBroadcastReceiver : BaseBroadcastListener<SmsObject?>() {
    @SuppressLint("NewApi")
    override fun onReceive(context: Context, intent: Intent) {
        val bundle = intent.extras
        val format = bundle!!.getString("format")
        val pdus = bundle["pdus"] as Array<Any>?
        if (pdus != null) {
            val msgs = arrayOfNulls<SmsMessage>(pdus.size)
            for (i in msgs.indices) {
                if (AndroidHelper().isGreaterThanOrEquals(Build.VERSION_CODES.M)) {
                    msgs[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray, format)
                } else {
                    msgs[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                }
                notifyObservers(getSmsObjectFromSmsMessage(msgs[i]))
                println(msgs[i]?.getMessageBody())
            }
        }
    }

    private fun getSmsObjectFromSmsMessage(message: SmsMessage?): SmsObject {
        val smsObject = SmsObject()
        smsObject.originatingAddress = message!!.originatingAddress
        smsObject.content = message.messageBody
        smsObject.serviceCenterAddress = message.serviceCenterAddress
        return smsObject
    }
}