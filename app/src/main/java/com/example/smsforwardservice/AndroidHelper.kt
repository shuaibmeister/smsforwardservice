package com.example.smsforwardservice

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.telephony.TelephonyManager
import android.util.Patterns
import java.text.SimpleDateFormat
import java.util.*


class AndroidHelper {
    val versionInt: Int
        get() = Build.VERSION.SDK_INT

    fun isGreaterThan(versionInt: Int): Boolean {
        return this.versionInt > versionInt
    }

    fun isGreaterThanOrEquals(versionInt: Int): Boolean {
        return this.versionInt >= versionInt
    }

    @SuppressLint("MissingPermission")
    fun getOwnLine1Number(context: Context): String {
        return (context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).line1Number
    }

    fun getTimeStringNow(): String{
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        return sdf.format(Date())
    }

    fun getApplicationId(context: Context): String{
        return context.packageName
    }

    fun getStorageSecretKey(): String{
        return BuildConfig.STORAGE_SECRET;
    }

    fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}